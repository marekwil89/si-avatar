import 'babel-polyfill';
import express from 'express';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import cors from 'cors';
import PaymentController from "./controllers/PaymentController";
import ImageController from "./controllers/ImageController";

const app = express();

app.use('/public', express.static('public'));
app.use(cors());

app.use(bodyParser.json());
app.use(fileUpload({
  useTempFiles : true,
  tempFileDir : 'public/uploads/'
}));

app.use('/payment', PaymentController);
app.use('/image', ImageController);

app.listen(5010, () => console.log('app is running on port 5010'));
