import express from 'express';
import {validationResult} from 'express-validator';
import {ValidatorFee} from "../validators/ValidatorFee";
import Stripe from 'stripe'
import {sendMail} from "../helpers/sendMail";
import {STRIPE_TEST_KEY} from "../helpers/CONSTS";
import {ValidatorSuccess} from "../validators/ValidatorSuccess";


const stripe = new Stripe(STRIPE_TEST_KEY, null);

const router = express.Router();

router.post('/fee', ...ValidatorFee, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    // const response = await sendMail({
    //   from: 'marekwil89@marekwil89.usermd.net',
    //   to: req.body.email,
    //   bcc: 'raven8912@gmail.com',
    //   subject: 'Nowe zamówienie ze strony AI.POSTER',
    //   html: `
    //   <h2>Nowe zamówienie ze strony AI.POSTER</h2>
    //
    //   <h3>Status: Przyjęto do realizacji</h3>
    //
    //   <p>
    //     Zamówiony obraz sprawdzisz pod <a href="${req.body.src}">tym</a> linkiem
    //   </p>
    //   <p>
    //     Podany numer telefonu ${req.body.phone}
    //   </p>
    //   <ul>
    //     <h4>Obraz zostanie dostarczony na adres:</h4>
    //     <li>${req.body.street}</li>
    //     <li>${req.body.post}, ${req.body.city} </li>
    //   </ul>
    // `,
    // });

    const session = await stripe.checkout.sessions.create({
      line_items: [
        {
          price: 'price_1MvEq4CXyPAvub73QGfaC6xg',
          quantity: 1,
        },
      ],
      mode: 'payment',
      // custom_fields: [
      //   {
      //     key: 'street',
      //     label: {type: 'custom', custom: 'Personalized engraving'},
      //     type: 'text',
      //   },
      // ],
      customer_email: req.body.email,
      success_url: `${req.get('origin')}/payment/success/?email=${req.body.email}&src=${req.body.src}`,
      cancel_url: `${req.get('origin')}/payment/cancel/`,
    });

    return res.status(200).json(session.url);
  } catch (e) {
    console.log(e);
    return res.status(400).json({errors: [{msg: "PAY_ERROR"}]});
  }
});


router.post('/success/:email/:src', ...ValidatorSuccess, async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    await sendMail({
      from: 'marekwil89@marekwil89.usermd.net',
      to: req.body.email,
      bcc: 'raven8912@gmail.com',
      subject: 'Nowe zamówienie ze strony AI.POSTER',
      html: `
        <h3>Nowe zamówienie ze strony AI.POSTER</h3>
        <p>Status: Przyjęto do realizacji</p>
        <p>Zamówiony obraz sprawdzisz pod <a href="${req.body.src}">tym</a> linkiem</p>
    `,
    });

    return res.status(200).send(true);
  } catch (e) {
    console.log(e);
    return res.status(400).json({errors: [{msg: "SUCCESS_ERROR"}]});
  }
});

export default router;