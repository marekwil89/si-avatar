import express from 'express';
// import {Configuration, OpenAIApi} from "openai";
import fs from "fs";
import fetch from 'node-fetch'
const cloudinary = require('cloudinary').v2;

cloudinary.config({
  cloud_name: "dbpfyuv8y",
  api_key: "482365313844252",
  api_secret: "X0XoOF21lTEHbEx4dElOCDQJOUM"
});

import Replicate from "replicate";
import {UPLOAD_URL} from "../helpers/CONSTS";
import {ValidatorGenerateReplicate} from "../validators/ValidatorGenerateReplicate";
import {validationResult} from "express-validator";

const router = express.Router();

// console.log('tutaj', fetch)

globalThis.fetch = fetch

const replicate = new Replicate({
  auth: '6405092c2867ebede66da661eaab3747c57c053a',
});



//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

// const configuration = new Configuration({
//   apiKey: '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',
// });

// const openai = new OpenAIApi(configuration);

// router.post('/generate', async (req, res) => {
//
//   console.log(req.body);
//
//   const response = await openai.createImage({
//     prompt: '19-year-old woman” by Steve McCurry, 35mm, F/2.8, insanely detailed and intricate, character, hypermaximalist, elegant, ornate, beautiful, exotic, revealing, appealing, attractive, amative, hyper-realistic, super detailed, popular on Flickr',
//     n: 4,
//     size: "512x512",
//   });
//
//   const image = response.data;
//
//
//   res.json(image)
// });

const model = 'mbentley124/openjourney-img2img:c49a9422a0d4303e6b8a8d2cf35d4d1b1fd49d32b946f6d5c74b78886b7e5dc3'

router.post('/generate-replicate', ValidatorGenerateReplicate, async (req, res) => {
  try {
    const response = await cloudinary.uploader
      .upload(req.files.avatar.tempFilePath,
        {
          public_id: req.files.avatar.name,
          eager: [
            {height: 480, crop: "fill"}
          ]
        });


    const output = await replicate.run(model,
       {
         input: {
           image: response.eager[0].url,
           prompt: 'greg rutkowski, cartoon style, trending on artstation',
           negative_prompt: 'deformed, blotches, blurry, saturated, maximalist, cripple, ugly, additional arms, additional legs, additional head, two heads, multiple people, group of people, blur',
           strength: 0.5,
           guidance_scale: 12.5,
           num_images_per_prompt: 1
         }
       });
    //
    await cloudinary.uploader.destroy(req.files.avatar.name);

    res.json(output);
  } catch (e) {
    console.log(e);
    return res.status(401).send('Ups, something want wrong');
  }
});



export default router;