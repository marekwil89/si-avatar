import {body} from "express-validator";

const availableTypes = ['image/jpeg', 'image/png'];

// export const ValidatorGenerateReplicate = [
//   body('').custom((value, { req }) => {
//     console.log('exist', !(req.files && req.files.file))
//     console.log('size', req.files.file.size >= 282165)
//     console.log('type', !availableTypes.includes(req.files.file.mimetype))
//
//     if(!(req.files && req.files.file)) {
//       throw new Error('File required');
//     }
//
//     if(req.files.file.size >= 282165) {
//       throw new Error('image is too big');
//     }
//     if(!availableTypes.includes(req.files.file.mimetype)) {
//       throw new Error('Bad file type');
//     }
//   }),
// ]

export const ValidatorGenerateReplicate = (req, res, next) => {
  if(!(req.files && req.files.avatar)) {
    return res.status(401).send('File required');
  }

  if(!availableTypes.includes(req.files.avatar.mimetype)) {
    return res.status(401).send('Bad file type');
  }

  next();
}

// function isLoggedIn(req, res, next) {
//   if (req.user) {
//     next();
//   } else {
//     // return unauthorized
//     res.send(401, "Unauthorized");
//   }
// };