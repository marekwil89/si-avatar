import {body} from "express-validator";

export const ValidatorFee = [
  body('src').isLength({min: 1}).withMessage('SRC_IS_EMPTY'),
  body('size').isLength({min: 1}).withMessage('SIZE_IS_EMPTY'),
  body('email').isLength({min: 1}).withMessage('EMAIL_IS_EMPTY'),
  body('post').isLength({min: 1}).withMessage('POST_IS_EMPTY'),
  body('city').isLength({min: 1}).withMessage('CITY_IS_EMPTY'),
  body('phone').isLength({min: 1}).withMessage('PHONE_IS_EMPTY'),
  body('street').isLength({min: 1}).withMessage('STREET_IS_EMPTY'),
]