import {body} from "express-validator";

export const ValidatorSuccess = [
  body('email').isEmail().withMessage('EMAIL_IS_EMPTY'),
  body('src').isLength({min: 1}).withMessage('SRC_IS_EMPTY'),
]