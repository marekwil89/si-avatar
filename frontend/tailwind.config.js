const colors = require("tailwindcss/colors");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      dark: '#272a29',
      gray: '#586F79',
      light: '#ECEBEA',
      white: '#ffffff',
      smoke: '#f7f8fb',
      slate: '#cbd5e1',
      // primary: "#e94c89",
      // primary: "#7e4ded",
      // primary: "#599af2",
      // primary: "#f18393",
      primary: "#237da0",
      danger: '#df5066'
    },
    extend: {},
  },
  plugins: [],
}
