import './index.scss';
import {
  BrowserRouter as Router,
  Routes ,
  Route,
} from "react-router-dom";
import Navigation from "./modules/navigation/Navigation";
import {ToastContainer} from "react-toastify";
import Footer from "./modules/footer/Footer";
import {lazy, Suspense} from "react";
import Loading from "./modules/loading/Loading";
import Success from "./pages/Payment/Success";
//
// const Avatar = lazy(() => {
//   return new Promise(resolve => setTimeout(resolve, 5 * 1000)).then(
//     () =>
//       Math.floor(Math.random() * 10) >= 4
//         ? import("./pages/Avatar/Avatar")
//         : Promise.reject(new Error())
//   );
// });

const Picture = lazy(() => import('./pages/Picture/Picture'));
const Avatar = lazy(() => import('./pages/Avatar/Avatar'));
const Home = lazy(() => import('./pages/Home/Home'));

function App() {

  return (
    <>
      <Router>
        <Navigation />
        <main className="flex flex-col">
          <Suspense fallback={
            <div className="flex justify-center items-center">
              <Loading className="mx-auto" />
            </div>
          }>
            <Routes>
              <Route exact path='/' element={<Home/>}/>
              <Route exact path='/picture/create' element={<Picture/>}/>
              <Route exact path='/avatar/create' element={<Avatar/>}/>
              <Route exact path='/payment/success' element={<Success />}/>
            </Routes>
          </Suspense>
        </main>

        <Footer />
        <ToastContainer />
      </Router>
    </>
  );
}

export default App;
