import Loading from "../../../modules/loading/Loading";

function AvatarFormPicture({loading, src, generate}) {
  return (
    <div className="flex flex-col items-center justify-center">
      {loading && (
        <Loading />
      )}
      <figure className="mb-0 w-full lg:w-full flex items-center flex-col justify-center ">
        {src && !loading && <img className="w-full shadow-lg" src={src} alt={'preview'} />}
        <button disabled={loading}
                onClick={generate}
                type="button"
                className="btn btn-sm btn-primary mb-5 mt-6">
          {loading ? 'Ładuję' : 'Generuj'}
          <span className="material-symbols-outlined ml-2">
            polyline
          </span>
        </button>
        <p className="text-base lg:text-lg text-gray font-normal text-center">
          Kliknij generuj aby stworzyć obraz za pomocą <span className="text-primary font-bold"> SI.</span>
        </p>
      </figure>
    </div>
  );
}

export default AvatarFormPicture;
