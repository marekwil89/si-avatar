import deepai from 'deepai';
import {useEffect, useRef, useState} from "react";
import Modal from "../../../modules/modal/Modal";
import {sizes, stylesApi} from "../../../helpers/CONSTS";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {errorsHandler} from "../../../helpers/errorHandler";
import Steps from "../../../modules/steps/Steps";
import {environment} from "../../../helpers/environment";
import axios from "axios";
import AvatarFormPicture from "./AvatarFormPicture";
import {upload} from "@testing-library/user-event/dist/upload";
import loader from '../../../assets/images/bars.svg';

const validation = Yup.object({
  text: Yup.string().max(50, 'Must be 50 characters or less').required('Required'),
})

const initialValues = {
  // text: 'house on the hill',
  // url: stylesApi[0].url,
  // size: sizes[0].name,
  email: 'raven8912@gmail.com',
  street: 'Pl. Czarnieckigo 44/1',
  city: 'Sulejówek',
  post: '05-070',
  phone: '881-469-849',
}

function AvatarForm() {
  const [avatarSrc, setAvatarSrc] = useState('');
  const input = useRef();
  const [preview, setPreview] = useState('');
  const [src, setSrc] = useState('');
  const [price, setPrice] = useState(sizes[0].price);
  // const [stepsModal, setStepsModal] = useState({
  //   status: false,
  //   step: 0
  // });
  const [orderModal, setOrderModal] = useState(false);
  const [loading, setLoading] = useState(false);

  // useEffect(() => {
  //   deepai.setApiKey('1e4570ba-cf45-4736-bebe-d6c39f8c1e2d');
  // }, [])

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validation}
        onSubmit={async (values) => {
          console.log('1232123');
          alert('zamawiam');
          // console.log('1231312', values);
          // setLoading(true)
          //
          // try {
          //   const response = await axios.post(
          //     `${environment.BACKEND_API}/payment/fee/`,
          //     {
          //       ...values,
          //       src
          //     },
          //   );
          //
          //   console.log(response.data);
          //
          //   if(response.data) {
          //     window.location.href = response.data;
          //   }
          //
          //   // if (response.data?._id) {
          //   //   setAlreadyLogged(response.data);
          //   //   toast(`Twój profil został zaktualizowany`);
          //   // }
          //   setLoading(false)
          // } catch (e) {
          //   setLoading(false)
          //   errorsHandler(e);
          // }
        }}
      >
        {({ values }) => {

          const handleUpload = (e) => {
            setPreview(URL.createObjectURL(e.target.files[0]))
          }

          const generate = async () => {
            setLoading(true);

            if(input.current?.files[0]) {
              try {

                const formData = new FormData();
                formData.append("avatar", input.current?.files[0]);

                const response = await axios.post(
                  `${environment.BACKEND_API}/image/generate-replicate/`,
                  formData,
                );

                if(response.data && response.data[0]) {
                  setSrc(response.data[0])
                }

                setLoading(false)
              } catch (e) {
                setLoading(false)
                errorsHandler(e);
              }
            }
            setLoading(false)
          }

          return (
          <Form>
            <div className="grid grid-cols-2 min-h-[84vh] relative">
              <aside className="pr-20 pl-10 py-10 border-r border-slate relative flex justify-center items-center">
                <label className="min-h-[300px] flex w-full  bg-smoke border border-slate border-dashed flex flex-col items-center justify-center">
                  <input className="absolute opacity-0" ref={input} onChange={handleUpload} type="file"/>
                  {!preview && (
                      <>
                        <h3 className="text-dark font-bold  text-2xl mb-0">Drop image</h3>
                        <p className="text-sm mt-1 text-gray text-right mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                      </>
                  )}


                  {preview && (
                    <img className="w-full" src={preview} alt="preview" />
                  )}
                </label>

                {!src ? (
                  <button className="border bg-white border-slate rounded-[50%] text-dark uppercase font-bold !absolute w-[100px] h-[100px] top-[45%] translate-y-[-50] right-[-50px]" onClick={generate}>
                    {loading ? (
                    <div className="p-7">
                        <img className="w-full" src={loader} />
                    </div>
                    ) : 'Generuj'}
                  </button>
                ) : (
                  <button onClick={() => setOrderModal(true)} className="border bg-primary border-primary rounded-[50%] text-white uppercase font-bold !absolute w-[100px] h-[100px] top-[45%] translate-y-[-50] right-[-50px]">
                    Zamów
                  </button>
                )}
              </aside>

              <aside className="pl-20 px-10 py-10 flex items-center justify-center">
                {src && (
                    <img className="w-full border border-slate border-dashed" src={src} alt="preview" />
                )}
              </aside>
            </div>

            <Modal show={orderModal}>
              <div>
                <h2 className="text-dark font-bold uppercase text-2xl mb-2">
                  Zamów  <span className="font-roboto text-primary">to gówno</span>
                </h2>

                <div className="grid grid-cols-2 gap-6">

                  <div className="relative col-span-2">
                    <label className="text-gray text-sm">
                      Email
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="email" type="email" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="email" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      City
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="city" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="city" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      Street
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="street" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="street" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      Phone
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="phone" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="phone" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      Post code
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="post" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="post" />
                  </div>

                </div>

                <div className="mt-6">

                </div>

              </div>
              <footer className="flex justify-between mt-6 border-t border-slate pt-5">
                <button onClick={() => setOrderModal(false)} type="button" className="btn btn-outline-slate btn-sm mr-4">
                  Zamknij
                </button>
                <aside className="flex items-center">
                  <p className="text-2xl font-bold text-dark text-right mr-4">{price} PLN</p>
                  <button disabled={loading} type="submit" className="btn btn-primary btn-sm">
                    Zamawiam
                  </button>
                </aside>

              </footer>
            </Modal>
          </Form>
        )}}
      </Formik>
    </>
  );
}

export default AvatarForm;
