import AvatarForm from "./AvatarForm/AvatarForm";

function Avatar() {
  return (
    <>
      <section className="container mx-auto">
        <AvatarForm />
      </section>
    </>
  );
}

export default Avatar;
