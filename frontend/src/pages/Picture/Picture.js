import PictureForm from "./PictureForm/PictureForm";

function Picture() {
  return (
    <>
      <section>
        <PictureForm />
      </section>
    </>
  );
}

export default Picture;
