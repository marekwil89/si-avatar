import deepai from 'deepai';
import {useEffect, useState} from "react";
import Modal from "../../../modules/modal/Modal";
import {sizes, stylesApi} from "../../../helpers/CONSTS";
import {ErrorMessage, Field, Form, Formik} from "formik";
import * as Yup from "yup";
import {errorsHandler} from "../../../helpers/errorHandler";
import Steps from "../../../modules/steps/Steps";
import {environment} from "../../../helpers/environment";
import axios from "axios";
import PictureFormImage from "./PictureFormImage";

const validation = Yup.object({
  text: Yup.string().max(50, 'Must be 50 characters or less').required('Required'),
})

const initialValues = {
  text: 'house on the hill',
  url: stylesApi[0].url,
  size: sizes[0].name,
  email: 'raven8912@gmail.com',
  street: 'Pl. Czarnieckigo 44/1',
  city: 'Sulejówek',
  post: '05-070',
  phone: '881-469-849',

}

function PictureForm() {
  const [src, setSrc] = useState('');
  const [price, setPrice] = useState(sizes[0].price);
  const [stepsModal, setStepsModal] = useState({
    status: false,
    step: 0
  });
  const [orderModal, setOrderModal] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    deepai.setApiKey('1e4570ba-cf45-4736-bebe-d6c39f8c1e2d');
  }, [])

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validation}
        onSubmit={async (values) => {
          console.log('1231312', values);
          setLoading(true)

          try {
            const response = await axios.post(
              `${environment.BACKEND_API}/payment/fee/`,
              {
                ...values,
                src
              },
            );

            console.log(response.data);

            if(response.data) {
              window.location.href = response.data;
            }

            // if (response.data?._id) {
            //   setAlreadyLogged(response.data);
            //   toast(`Twój profil został zaktualizowany`);
            // }
            setLoading(false)
          } catch (e) {
            setLoading(false)
            errorsHandler(e);
          }
        }}
      >
        {({ values }) => {
          const handleSize = (e) => {
            const size = sizes.find(elem => elem.name === e.target.value);
            if(size) {
              setPrice(size.price)
            }
          }

          const generate = async () => {
            setLoading(true)

            try {
              const response = await deepai.callStandardApi(values.url, {
                text: values.text,
                grid_size: "1",
                width: "1024",
                height: "512"
              });

              if (response?.output_url) {
                setSrc(response?.output_url);
              }

              setLoading(false);
            } catch (e) {
              setLoading(false)
              errorsHandler(e);
            }
          }

          return (
          <Form>
            <div className="grid lg:grid-cols-3 gap-0">
              <aside className="lg:col-span-1 lg:h-[82.5vh] lg:overflow-y-scroll lg:border-r border-slate">
                <div className="lg:px-8 px-4 py-10 border-b border-slate">
                  <h3 className="text-dark font-bold uppercase text-sm mb-4 flex items-start">
                    <span className="text-primary mr-2">1.</span>
                    <span>
                      Wpisz tekst opisujący to co chcesz wygenerować
                      <button onClick={() => setStepsModal({status: true, step: 0})} type="button" className="material-symbols-outlined text-lg translate-y-[3px] text-primary ml-1">info</button>
                    </span>

                  </h3>
                  <div className="relative">
                    <Field className="border border-slate w-full p-3 text-sm text-dark" placeholder="Wpisz dowolną frazę aby stworzyć..." name="text" type="text" />
                    <div className="absolute top-[100%] left-0 text-primary"><ErrorMessage name="text" /></div>
                  </div>
                </div>

                <div className="lg:px-8 px-4 py-10 border-b border-slate">
                  <h3 className="text-dark font-bold uppercase text-sm mb-4 flex items-center">
                    <span className="text-primary mr-2">2.</span>
                    <span>
                      Wybierz styl
                      <button onClick={() => setStepsModal({status: true, step: 1})} type="button" className="material-symbols-outlined text-lg translate-y-[3px] text-primary ml-1">info</button>
                    </span>
                  </h3>

                  <div className="grid xl:grid-cols-3 grid-cols-2 gap-4">
                    {stylesApi?.map((elem, key) => (
                      <label key={key} className={`btn btn-xs justify-between ${values.url === elem.url ? 'btn-primary' : 'btn-outline-slate'} w-full`}>
                        {elem.name}
                        <Field className="opacity-0 absolute z-[-10]" type="radio" name="url" value={elem.url} />
                      </label>
                    ))}
                  </div>
                </div>

                <div className="lg:px-8 px-4 py-10">
                  <h3 className="text-dark font-bold uppercase text-sm mb-4 flex items-center">
                    <span className="text-primary mr-2">3.</span>
                    <span>
                      Wybierz rozmiar
                      <button onClick={() => setStepsModal({status: true, step: 3})} type="button" className="material-symbols-outlined text-lg translate-y-[3px] text-primary ml-1">info</button>
                    </span>
                  </h3>
                  <div className="grid grid-cols-2 gap-4">
                    {sizes?.map((elem, key) => (
                      <label key={key} onChange={e => handleSize(e)} className={`btn btn-xs ${values.size === elem.name ? 'btn-primary' : 'btn-outline-slate'} w-full`}>
                        {elem.name}
                        <Field className="opacity-0 absolute z-[-10]" type="radio" name="size" value={elem.name} />
                      </label>
                    ))}
                  </div>
                </div>
              </aside>

              <article className="lg:p-10 p-4 bg-smoke lg:col-span-2">
                <PictureFormImage src={src} loading={loading} generate={generate} />
              </article>
            </div>

            <div className="flex grid-cols-3 border border-slate justify-end lg:px-6 px-4 py-4">
              <div className="flex items-end mr-4">
                <p className="text-gray font-normal text-sm mr-2 translate-y-[-3px]">
                  Cena
                </p>
                <p className="text-2xl font-bold text-dark">{price} PLN</p>
              </div>

              <button disabled={loading}
                      onClick={() => setOrderModal(true)}
                      type="button"
                      className="btn btn-xs btn-primary">
                Zamów
                <span className="material-symbols-outlined ml-2">
                  shopping_cart
                </span>
              </button>
            </div>

            <Modal show={orderModal}>
              <div>
                <h2 className="text-dark font-bold uppercase text-2xl mb-2">
                  Sprawdź przykłady  <span className="font-roboto text-primary">najlepszych prac</span>
                </h2>

                <div className="grid grid-cols-2 gap-6">

                  <div className="relative col-span-2">
                    <label className="text-gray text-sm">
                      Email
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="email" type="email" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="email" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      City
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="city" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="city" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      Street
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="street" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="street" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      Phone
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="phone" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="phone" />
                  </div>

                  <div className="relative">
                    <label className="text-gray text-sm">
                      Post code
                      <Field className="border border-slate w-full p-3 text-sm text-dark" name="post" type="text" />
                    </label>
                    <ErrorMessage className="absolute top-[100%] left-0 text-primary" name="post" />
                  </div>

                </div>
              </div>
              <footer className="flex justify-end mt-6 border-t border-slate pt-5">
                <button onClick={() => setOrderModal(false)} type="button" className="btn btn-outline-slate btn-sm mr-4">
                  Zamknij
                </button>
                <button disabled={loading} type="submit" className="btn btn-primary btn-sm">
                  Zamawiam
                </button>
              </footer>
            </Modal>
          </Form>
        )}}
      </Formik>


      <Modal show={stepsModal.status}>
        <Steps currentStep={stepsModal.step} />
        <footer className="flex justify-end mt-6 border-t border-slate pt-5">
          <button onClick={() => setStepsModal({step: 0, status: false})} className="btn btn-outline-slate btn-sm">
            Zamknij
          </button>
        </footer>
      </Modal>
    </>
  );
}

export default PictureForm;
