import {Link, useSearchParams} from "react-router-dom";
import {useEffect} from "react";
import axios from "axios";
import {environment} from "../../helpers/environment";
import {errorsHandler} from "../../helpers/errorHandler";

function Success() {
  const [searchParams] = useSearchParams();

  useEffect (() => {
    const fetchData = async () => {
      try {
        const response = await axios.post(
          `${environment.BACKEND_API}/payment/success/${searchParams.get('email')}/${searchParams.get('src')}`,
          {
            email: searchParams.get('email'),
            src: searchParams.get('src')
          }
        );

        console.log(response);
      } catch (e) {
        errorsHandler(e);
      }
    };
    if(searchParams.get('email')?.includes('@') && searchParams.get('src')) {
      fetchData().then(() => null).catch(console.error);
    }
  }, [searchParams])

  return (
    <>
      <section className="py-10 flex">
        <div className="container mx-auto px-6">
          <article className="flex flex-col justify-center">
            <h1 className="text-dark font-bold uppercase text-2xl lg:text-4xl mb-4 text-center">
              Zamówienie
            </h1>
            <div className="mb-6">
              <p className="text-base text-gray font-normal text-center">
                Twoje zamówienie zostało przesłane do <span className="text-primary font-bold">AI.PICTURE</span>
              </p>
              <p className="text-base text-gray font-normal text-center">
                Na twój email  przesłaliśmy podsumowanie zamówienia
              </p>
            </div>

            <div className="flex items-center justify-center">
              <Link to={'/create'} className="btn btn-sm btn-primary">
                Powrót
              </Link>
            </div>
          </article>
        </div>
      </section>
    </>
  );
}

export default Success;
