import headImage from '../../assets/images/home-header.jpg';
import contactImage from '../../assets/images/contact-lead.jpg';
import {Link} from "react-router-dom";
import Featured from "../../modules/featured/Featured";
import Modal from "../../modules/modal/Modal";
import {useState} from "react";

function Home() {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <section className="py-10 flex">
        <div className="container mx-auto px-6">
          <div className="grid lg:grid-cols-2 gap-8">
            <article className="flex flex-col justify-center">
              <h1 className="text-dark font-bold uppercase text-2xl lg:text-4xl">
                To najlepsze miejsce by stworzyć obraz pomocą <span className="text-primary">Sztucznej&nbsp;inteligencji</span>
              </h1>
              <p className="text-base lg:text-lg text-gray font-normal my-6">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ipsam nostrum omnis? Architecto iusto nesciunt reiciendis repudiandae soluta temporibus, veniam.
              </p>

              <div className="flex">
                <Link to={'create'} className="btn btn-sm btn-primary mr-3">
                  Stwórz
                </Link>
                <button className="btn btn-sm btn-outline-slate" onClick={() => setShowModal(true)}>
                  Jak to działa?
                </button>
              </div>
            </article>
            <aside className="hidden lg:block">
              <img className="w-full" src={headImage} />
            </aside>
          </div>
        </div>
      </section>

      <section className="py-20 bg-smoke">
        <div className="container mx-auto px-6">
          <header className="mb-10 text-center">
            <h2 className="text-dark font-bold uppercase text-2xl lg:text-3xl mb-2">
              Sprawdź przykłady  <span className="font-roboto text-primary">najlepszych prac</span>
            </h2>
            <p className="text-base lg:text-lg text-gray font-normal">Consequatur ipsam nostrum omnis? Architecto iusto nesciunt reiciendis repudiandae soluta temporibus, veniam.</p>
          </header>
          <Featured />
        </div>
      </section>

      <section className="py-10 flex">
        <div className="container mx-auto px-6">
          <div className="grid lg:grid-cols-2 gap-8">
            <aside>
              <img className="w-full" src={contactImage} />
            </aside>
            <article className="flex flex-col justify-center items-end">
              <h1 className="text-dark font-bold uppercase text-2xl lg:text-4xl text-right">
                To najlepsze miejsce by stworzyć obraz pomocą <span className="text-primary">Sztucznej&nbsp;inteligencji</span>
              </h1>
              <p className="text-base lg:text-lg text-gray font-normal my-6 text-right">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ipsam nostrum omnis? Architecto iusto nesciunt reiciendis repudiandae soluta temporibus, veniam.
              </p>

              <div className="flex">
                <Link to={'create'} className="btn btn-sm btn-primary mr-3">
                  Stwórz
                </Link>
                <Link to={'create'} className="btn btn-sm btn-outline-slate">
                  Kontakt
                </Link>
              </div>
            </article>

          </div>
        </div>
      </section>

      <section className="py-20 bg-smoke">
        <article className="flex flex-col justify-center items-center lg:w-[800px] px-6 mx-auto">
          <h1 className="text-dark font-bold uppercase text-2xl lg:text-4xl text-center">
            To najlepsze miejsce by stworzyć obraz pomocą <span className="text-primary">Sztucznej&nbsp;inteligencji</span>
          </h1>
          <p className="text-base lg:text-lg text-dark font-normal my-6 text-center">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur ipsam nostrum omnis? Architecto iusto nesciunt reiciendis repudiandae soluta temporibus, veniam.
          </p>
          <Link to={'create'} className="btn btn-sm btn-primary">
            Stwórz
          </Link>
        </article>
      </section>

      <Modal show={showModal}>
        <header className="mb-6">
          <h2 className="text-dark font-bold uppercase text-2xl mb-2">
            Sprawdź przykłady  <span className="font-roboto text-primary">najlepszych prac</span>
          </h2>

          <p className="text-base lg:text-sm text-gray font-normal">Consequatur ipsam nostrum omnis? Architecto iusto nesciunt reiciendis repudiandae soluta temporibus, veniam.</p>
        </header>
        <Featured />
        <footer className="flex justify-end mt-6 border-t border-slate pt-5">
          <button onClick={() => setShowModal(false)} className="btn btn-outline-slate btn-sm mr-5">
            Zamknij
          </button>
          <Link className="btn btn-sm btn-primary" to="/picture/create">Zaczynamy</Link>
        </footer>
      </Modal>
    </>
  );
}

export default Home;
