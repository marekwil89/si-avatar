import {useState} from "react";
import Featured from "../featured/Featured";


function Steps({currentStep}) {
  const [step, setStep] = useState(currentStep || 0);


  return (
    <>
      <header className="grid lg:grid-cols-6 grid-cols-3 gap-4 mb-6">
        <button onClick={() => setStep(0)} className={`btn btn-xs ${0 === step ? 'btn-primary' : 'btn-outline-slate'} w-full`}>
          Krok 1
        </button>
        <button onClick={() => setStep(1)} className={`btn btn-xs ${1 === step ? 'btn-primary' : 'btn-outline-slate'} w-full`}>
          Krok 2
        </button>
        <button onClick={() => setStep(2)} className={`btn btn-xs ${2 === step ? 'btn-primary' : 'btn-outline-slate'} w-full`}>
          Krok 3
        </button>
      </header>
        {step === 0 && (
          <article className="mb-6">
            <h2 className="text-dark font-bold uppercase text-2xl mb-2">
              Wpisz <span className="font-roboto text-primary">tekst</span>
            </h2>

            <p className="text-base lg:text-sm text-gray font-normal">
              opisujący to co chcesz zobaczyć na obrazku Sprawdź przykłady
            </p>
          </article>
        )}
        {step === 1 && (
          <>
            <article className="mb-6">
              <h2 className="text-dark font-bold uppercase text-2xl mb-2">
                Wybierz <span className="font-roboto text-primary">styl</span>
              </h2>

              <p className="text-base lg:text-sm text-gray font-normal">
                Kliknij na kafelek z nazwą stylu aby zobaczyć przykłady obrazków wygenerowanych w danej kategorii
              </p>
            </article>
            <Featured />
          </>
        )}
        {step === 2 && (
          <article className="mb-6">
            <h2 className="text-dark font-bold uppercase text-2xl mb-2">
              Wybierz <span className="font-roboto text-primary">rozmiar</span>
            </h2>

            <p className="text-base lg:text-sm text-gray font-normal">
              Wybierz rozmiar jaki Cię interesuje, należy pamiętać że podane rozmiary są w cm
            </p>
          </article>
        )}
    </>
  );
}

export default Steps;
