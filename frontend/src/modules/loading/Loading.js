function Loading() {
  return (
    <div className="w-16 h-16 rounded-full animate-spin border-y-2 border-solid border-primary border-t-transparent"></div>
  );
}

export default Loading;
