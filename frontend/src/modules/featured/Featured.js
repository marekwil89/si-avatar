import anime1 from '../../assets/images/anime-featured-1.jpeg'
import anime2 from '../../assets/images/anime-featured-2.jpeg'
import anime3 from '../../assets/images/anime-featured-3.jpeg'
import anime4 from '../../assets/images/anime-featured-4.jpeg'
import fantasy1 from '../../assets/images/fantasy-featured-1.jpeg'
import fantasy2 from '../../assets/images/fantasy-featured-2.jpeg'
import fantasy3 from '../../assets/images/fantasy-featured-3.jpeg'
import fantasy4 from '../../assets/images/fantasy-featured-4.jpeg'
import future1 from '../../assets/images/future-featured-1.jpeg'
import future2 from '../../assets/images/future-featured-2.jpeg'
import future3 from '../../assets/images/future-featured-3.jpeg'
import future4 from '../../assets/images/future-featured-4.jpeg'
import old1 from '../../assets/images/old-featured-1.jpeg'
import old2 from '../../assets/images/old-featured-2.jpeg'
import old3 from '../../assets/images/old-featured-3.jpeg'
import old4 from '../../assets/images/old-featured-4.jpeg'
import machine1 from '../../assets/images/machine-featured-1.jpeg'
import machine2 from '../../assets/images/machine-featured-2.jpeg'
import machine3 from '../../assets/images/machine-featured-3.jpeg'
import machine4 from '../../assets/images/machine-featured-4.jpeg'
import street1 from '../../assets/images/street-featured-1.jpeg'
import street2 from '../../assets/images/street-featured-2.jpeg'
import street3 from '../../assets/images/street-featured-3.jpeg'
import street4 from '../../assets/images/street-featured-4.jpeg'
import {useState} from "react";
import {styles} from "../../helpers/CONSTS";

const featuredImages = [{
  category: styles.fantasy,
  image: fantasy1
},{
  category: styles.fantasy,
  image: fantasy2
},{
  category: styles.fantasy,
  image: fantasy3
},{
  category: styles.fantasy,
  image: fantasy4
},{
  category: styles.machine,
  image: machine1
},{
  category: styles.machine,
  image: machine2
},{
  category: styles.machine,
  image: machine3
},{
  category: styles.machine,
  image: machine4
},{
  category: styles.old,
  image: old1
},{
  category: styles.old,
  image: old2
},{
  category: styles.old,
  image: old3
},{
  category: styles.old,
  image: old4
},{
  category: styles.future,
  image: future1
},{
  category: styles.future,
  image: future2
},{
  category: styles.future,
  image: future3
},{
  category: styles.future,
  image: future4
},{
  category: styles.anime,
  image: anime1
},{
  category: styles.anime,
  image: anime2
},{
  category: styles.anime,
  image: anime3
},{
  category: styles.anime,
  image: anime4
},{
  category: styles.street,
  image: street1
},{
  category: styles.street,
  image: street2
},{
  category: styles.street,
  image: street3
},{
  category: styles.street,
  image: street4
}];

const categories = Object.values(styles)

function Featured() {
  const [selectedCategory, setSelectedCategory] = useState(styles.fantasy);

  return (
    <>

      <div className="grid lg:grid-cols-8 grid-cols-4 lg:gap-4 gap-3 mb-5">
        {categories.map((elem, key) => (
          <button key={key} onClick={() => setSelectedCategory(elem)} className={`btn btn-xs ${elem === selectedCategory ? 'btn-primary' : 'btn-outline-slate'} w-full`}>
            {elem}
          </button>
        ))}
      </div>
      <div className="grid grid-cols-2 gap-8">
        {featuredImages.map((elem, key) => elem.category === selectedCategory && (
          <article key={key} className="border border-slate">
            <img className="w-full" src={elem.image} alt="preview" />
          </article>
        ))}
      </div>
    </>
  );
}

export default Featured;
