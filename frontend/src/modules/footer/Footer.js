function Footer() {
  return (
    <footer className="p-4 bg-dark min-h-[8vh] flex justify-center items-center">
      <p className="text-sm text-slate text-center uppercase">© 2023
        <a href="https://flowbite.com/" className="hover:underline"> AI.POSTER™</a>. All Rights Reserved.
      </p>
    </footer>
  );
}

export default Footer;


