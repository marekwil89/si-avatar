import {Link} from "react-router-dom";

function Navigation() {
  return (
    <nav className="min-h-[8vh] lg:px-6 px-4 left-0 top-0 w-full flex justify-between items-center border-b border-slate ">
      <Link className="font-logo text-dark text-2xl sm:text-3xl" to="/">
        AI.<span className="text-primary font-logo">PICTURE</span>
      </Link>
      <aside className="flex items-center">
        <Link className="btn text-xs btn-link text-sm mr-4" to="/">Główna</Link>
        <a className="btn text-xs btn-link text-sm mr-3 lg:mr-4" href="mailto:marekwil89@gmail.com">Kontakt</a>
        <Link type="button" className="btn btn-xs btn-primary" to="/avatar/create">
          Avatar
        </Link>
        <Link type="button" className="btn btn-xs btn-primary" to="/picture/create">
          Stwórz
        </Link>
      </aside>
    </nav>
  );
}

export default Navigation;
