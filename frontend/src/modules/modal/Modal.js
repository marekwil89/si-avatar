import React from "react";

const Modal = ({show = false, children}) => {
  return (
    show ? (
      <div className="flex justify-center items-start overflow-y-auto fixed left-0 top-0 w-full h-[100vh] py-3 lg:py-10 px-3 lg:px-3 ">
        <div className="fixed w-full h-full left-0 top-0 bg-dark z-40 opacity-70 z-0"></div>
        <article className="z-50 bg-white p-5 rounded-lg shadow-lg w-full lg:w-[1000px] relative">
          {children}
        </article>
      </div>
    ) : null
  );
};

export default Modal;
