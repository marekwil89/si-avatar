import {toast} from "react-toastify";

export const errorsHandler = (e) => {
  if(e?.response?.data?.err) {
    toast(e.response.data.err)
  }
}