export const styles = {
  machine: 'machine',
  fantasy: 'fantasy',
  anime: 'anime',
  old: 'old',
  future: 'future',
  street: 'street'
}

export const stylesApi = [
  {
    name: styles.machine,
    url: 'text2img'
  },
  {
    name: styles.fantasy,
    url: 'fantasy-world-generator'
  },
  {
    name: styles.anime,
    url: 'anime-world-generator'
  },
  {
    name: styles.old,
    url: 'old-style-generator'
  },
  {
    name: styles.future,
    url: 'future-architecture-generator'
  },
  {
    name: styles.street,
    url: 'street-art-generator'
  },
]

export const sizes = [{
  name: '60x42 CM',
  price: 120
},
//   {
//   name: '768x512',
//   price: 100
// }, {
//   name: '512x512',
//   price: 150
// }, {
//   name: '512x768',
//   price: 200
// }
];